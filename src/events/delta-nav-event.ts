interface DeltaNavEventDetails {
	route: string;
}

export class DeltaNavEvent extends CustomEvent<DeltaNavEventDetails> {

	static create( route: string ) : DeltaNavEvent {
		return new DeltaNavEvent( 'delta-nav-event', {
			bubbles: true,
			composed: true,
			detail: { route: route }
		});
	}

}

declare global {
	interface GlobalEventHandlersEventMap {
		'delta-nav-event': DeltaNavEvent;
	}
}
