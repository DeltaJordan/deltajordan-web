let g_isDarkMode = false;

export class Theme {

	static init() : void {
		switch( window.localStorage.getItem( 'delta-dark-mode' ) ) {
			case 'on':
				g_isDarkMode = true;
				break;
			case 'off':
				g_isDarkMode = false;
				break;
			default:
				g_isDarkMode = true;
				window.localStorage.setItem( 'delta-dark-mode', 'on' );
				break;
		}

		document.documentElement.classList.remove( g_isDarkMode ? 'sl-theme-light' : 'sl-theme-dark' );
		document.documentElement.classList.add( g_isDarkMode ? 'sl-theme-dark' : 'sl-theme-light' );
	}

	static get isDarkMode() : boolean {
		return g_isDarkMode;
	}

	static switchTheme() : void {
		g_isDarkMode = !g_isDarkMode;
		window.localStorage.setItem( 'delta-dark-mode', g_isDarkMode ? 'on' : 'off' );
		document.documentElement.classList.remove( g_isDarkMode ? 'sl-theme-light' : 'sl-theme-dark' );
		document.documentElement.classList.add( g_isDarkMode ? 'sl-theme-dark' : 'sl-theme-light' );
	}

}
