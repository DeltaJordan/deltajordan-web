import { css, CSSResultGroup, html } from 'lit';
import { customElement, state } from 'lit/decorators.js';
import { DeltaElement } from '../delta-element';
import '../components/delta-footer';
import '../components/portfolio/delta-floating-card';

@customElement('delta-home-page')
export class DeltaHomePage extends DeltaElement {
	@state()
	ready = false;

	rainRandom: Array<Array<number>> = [];

	override connectedCallback(): void {
		super.connectedCallback();
		this.initRain();
	}

	static override get styles(): CSSResultGroup {
		return css`
			:host {
				height: 100vh;
				text-align: center;
				display: flex;
				flex-flow: column;
			}

			.rain-container {
				flex: 1 1 auto;
				background-size: cover;
				background-repeat: no-repeat;
			}

			.rain-back {
				height: 100%;
			}

			.rain {
				position: absolute;
				left: 0;
				width: 100%;
				height: 100%;
				z-index: 2;
			}

			.rain.back-row {
				display: none;
				z-index: 1;
				bottom: 60px;
				opacity: 0.5;
			}

			.rain-container.back-row-toggle .rain.back-row {
				display: block;
			}

			.drop {
				position: absolute;
				bottom: 100%;
				width: 15px;
				height: 120px;
				pointer-events: none;
				animation: drop 0.5s linear infinite;
			}

			@keyframes drop {
				0% {
					transform: translateY(0vh);
				}
				75% {
					transform: translateY(90vh);
				}
				100% {
					transform: translateY(90vh);
				}
			}

			.stem {
				width: 1px;
				height: 60%;
				margin-left: 7px;
				background: linear-gradient(to bottom, rgba(255, 255, 255, 0), rgba(255, 255, 255, 0.25));
				animation: stem 0.5s linear infinite;
			}

			@keyframes stem {
				0% {
					opacity: 1;
				}
				65% {
					opacity: 1;
				}
				75% {
					opacity: 0;
				}
				100% {
					opacity: 0;
				}
			}

			.splat {
				width: 15px;
				height: 10px;
				border-top: 2px dotted rgba(255, 255, 255, 0.5);
				border-radius: 50%;
				opacity: 1;
				transform: scale(0);
				animation: splat 0.5s linear infinite;
				display: none;
			}

			.rain-container.splat-toggle .splat {
				display: block;
			}

			@keyframes splat {
				0% {
					opacity: 1;
					transform: scale(0);
				}
				80% {
					opacity: 1;
					transform: scale(0);
				}
				90% {
					opacity: 0.5;
					transform: scale(1);
				}
				100% {
					opacity: 0;
					transform: scale(1.5);
				}
			}

			.splat-toggle {
				top: 20px;
			}

			.back-row-toggle {
				top: 90px;
				line-height: 12px;
				padding-top: 14px;
			}

			.single-toggle {
				top: 160px;
			}

			.rain-container.single-toggle .drop {
				display: none;
			}

			.rain-container.single-toggle .drop:nth-child(10) {
				display: block;
			}

			.float-card {
				position: absolute;
				left: 35%;
				right: 35%;
				z-index: 3;
			}

			.back-credits {
				position: absolute;
				bottom: var(--sl-spacing-x-small);
				right: var(--sl-spacing-x-small);
				z-index: 4;
			}

			.footer {
				flex: 0 1 auto;
				z-index: 3;
			}
		`;
	}

	protected override render(): unknown {
		if (!this.ready) {
			return html`
				<delta-floating-card></delta-floating-card>
				<delta-footer></delta-footer>
			`;
		}

		/* eslint-disable indent */
		return html`
			<style>
				.rain-container {
					background-image: url('/assets/img/home-back.png');
				}
			</style>
			<div class="rain-container back-row-toggle splat-toggle">
				<div class="rain front-row">
					${ this.rainRandom.map((rainTuple) => {
			const increment = rainTuple[0];
			const duration = rainTuple[1];
			const altitude = rainTuple[2];
			return html`
					<div class="drop" style="left: ${ increment }%;
						bottom: ${ altitude + altitude - 1 + 100 }%;
						animation-delay: 0.${ duration }s;
						animation-duration: 0.5${ duration }s;">
						<div class="stem" style="animation-delay: 0.${ duration }s;
							animation-duration: 0.5${ duration }s;">
						</div>
						<div class="splat" style="animation-delay: 0.${ duration }s;
							animation-duration: 0.5${ duration }s;">
						</div>
					</div>
				`;
		}
		) }
				</div>
				<div class="rain back-row">
					${ this.rainRandom.map((rainTuple) => {
			const increment = rainTuple[0];
			const duration = rainTuple[1];
			const altitude = rainTuple[2];
			return html`
					<div class="drop" style="right: ${ increment }%;
						bottom: ${ altitude + altitude - 1 + 100 }%;
						animation-delay: 0.${ duration }s;
						animation-duration: 0.5${ duration }s;">
						<div class="stem" style="animation-delay: 0.${ duration }s;
							animation-duration: 0.5${ duration }s;">
						</div>
						<div class="splat" style="animation-delay: 0.${ duration }s;
							animation-duration: 0.5${ duration }s;">
						</div>
					</div>
				`;
		}
		) }
				</div>
				<delta-floating-card class="float-card"></delta-floating-card>
			</div>
			<div class="back-credits">Image by sofind on Freepik</div>
			<delta-footer class="footer"></delta-footer>
		`;
		/* eslint-enable indent */
	}

	initRain(): void {
		let increment = 0;

		while (increment < 100) {
			const randDuration = (Math.floor(Math.random() * (98 - 1 + 1) + 1));
			const randAltitude = (Math.floor(Math.random() * (2 - 2 + 1) + 2));
			increment += randAltitude;

			this.rainRandom.push([increment, randDuration, randAltitude]);
		}

		this.ready = true;
	}
}
