import { LitElement } from 'lit';
import { DeltaNavEvent } from './events/delta-nav-event';

export abstract class DeltaElement extends LitElement {
	protected navigate(route: string): void {
		this.dispatchEvent(DeltaNavEvent.create(route));
	}

	protected navAction(route: string): () => void {
		return this.navigate.bind(this, route);
	}

	protected redirect404(): void {
		this.dispatchEvent(
			new CustomEvent('delta-404-event', {
				bubbles: true,
				composed: true,
			})
		);
	}
}
