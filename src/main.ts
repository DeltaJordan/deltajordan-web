import '@shoelace-style/shoelace/dist/themes/light.css';
import '@shoelace-style/shoelace/dist/themes/dark.css';
import './themes/delta-light.css';
import './themes/delta-dark.css';
import './common.css';

import SlButton from '@shoelace-style/shoelace/dist/components/button/button';

(
	SlButton.prototype.constructor as unknown as { styles: { cssText: string } }
).styles.cssText += `
	slot[part="prefix"], slot[part="suffix"] {
		font-size: var(--sl-font-size-medium);
	}

	:host([size="small"]) slot[part="prefix"], :host([size="small"]) slot[part="suffix"] {
		font-size: var(--sl-font-size-small);
	}

	:host([size="large"]) slot[part="prefix"], :host([size="large"]) slot[part="suffix"] {
		font-size: var(--sl-font-size-large);
	}
`;

import { setBasePath } from '@shoelace-style/shoelace/dist/utilities/base-path.js';
import { Theme } from './util/theme';
import './components/delta-page';

setBasePath( '/shoelace' );
Theme.init();
