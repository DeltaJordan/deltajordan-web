import { css, CSSResultGroup, html } from 'lit';
import { customElement, state } from 'lit/decorators.js';
import { DeltaElement } from '../delta-element';
import { Theme } from '../util/theme';

@customElement('delta-navbar')
export class DeltaNavbar extends DeltaElement {
	@state()
	darkMode = Theme.isDarkMode;

	static override get styles(): CSSResultGroup {
		return css`
			:host {
				background-color: var(--sl-color-neutral-50);
			}

			.nav-container {
				margin: auto;
				display: flex;
				justify-content: space-between;
				flex-wrap: wrap;
				align-items: center;
				gap: 6px;
				max-width: 1200px;
			}

			.group {
				display: flex;
				justify-content: space-between;
				align-items: center;
				flex-grow: 1;
				gap: 6px;
			}

			.tight {
				justify-content: unset;
				flex-grow: 0;
			}

			.text-nav {
				display: flex;
				justify-content: space-between;
				align-items: center;
				flex-grow: 1;
				gap: 12px;
			}

			.text-nav > sl-button::part(base) {
				font-size: var(--sl-font-size-large);
				font-weight: var(--sl-font-weight-bold);
			}

			h1 {
				margin: 0;
				font-family: var(--sl-font-sans);
				color: var(--sl-color-primary-600);
				align-self: flex-end;
			}

			h1[data-dev] {
				color: var(--sl-color-danger-600);
			}

			h1[data-dev]::after {
				content: " (Dev)";
			}

			h1 > sdl-link::part(base) {
				color: inherit !important;
				text-decoration: none;
				cursor: pointer;
			}

			sl-input.search {
				flex-grow: 100000;
				max-width: 600px;
			}

			div.spacer {
				flex-grow: 1000;
				flex-shrink: 1000;
			}

			sl-menu {
				margin-top: var(--sl-spacing-2x-small);
			}

			sl-icon-button::part(base) {
				color: var(--sl-color-neutral-1000);
			}

			sl-button:not([slot="footer"])::part(base) {
				color: var(--sdl-text-color);
			}

			sl-button > sl-icon {
				font-size: var(--sl-font-size-x-large);
				vertical-align: middle;
			}

			sl-checkbox {
				margin-bottom: var(--sl-spacing-x-small);
			}

			sl-spinner {
				margin: auto;
			}

			sl-badge {
				translate: 5px -3px;
				z-index: 100;
			}

			sl-badge[hidden] {
				display: none;
			}

			sl-button.discord-login::part(base) {
				background-color: #5865f2;
				color: white;
			}

			.logo {
				width: 3rem;
				height: 3rem;
				margin: 10px 0;
				cursor: pointer;
			}

			.modqueue > sl-icon {
				font-size: var(--sl-font-size-x-large);
				vertical-align: middle;
			}

			.modqueue::part(label) {
				padding: 0 var(--sl-spacing-x-small);
			}

			@media only screen and (max-width: 540px) {
				:host {
					flex-wrap: nowrap;
				}

				sl-input.search {
					max-width: calc(
						100vw - (2 * var(--sl-spacing-2x-small)) - 24px -
							var(--sl-input-height-medium) - 4.5rem -
							(6 * var(--sl-spacing-x-small))
					);
				}
			}
		`;
	}

	protected override render(): unknown {
		return html`
			<div class="nav-container">
				<sl-icon
					class="logo"
					src="/assets/icons/delta.svg"
					@click=${ this.navAction('/') }
				></sl-icon>
			</div>
		`;
	}
}
