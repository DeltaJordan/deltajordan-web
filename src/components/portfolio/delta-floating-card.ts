import { html, css, CSSResultGroup } from 'lit';
import { customElement } from 'lit/decorators.js';
import { DeltaElement } from '../../delta-element';

@customElement('delta-floating-card')
export class DeltaFloatingCard extends DeltaElement {
	static override get styles(): CSSResultGroup {
		return css`
			:host {
				padding: 1em;
				display: flex;
				flex-flow: column;
				gap: var(--sl-spacing-x-small);
  				background-color: rgba(0, 0, 0, 0.95);
			}

			@supports (-webkit-backdrop-filter: none) or (backdrop-filter: none) {
				:host {
					-webkit-backdrop-filter: blur(10px);
					backdrop-filter: blur(10px);
					background-color: rgba(0, 0, 0, 0.5);
				}
			}

			sl-icon {
				font-size: var(--sl-font-size-large);
				cursor: pointer;
			}

			h2, h3 {
				margin: var(--sl-spacing-x-small);
			}

			table {
				align-self: center;
				border-collapse: separate;
				border-spacing: var(--sl-spacing-small) 0;
			}
		`;
	}

	protected override render(): unknown {
		return html`
			<h1>Jordan Marine</h1>
			<em style="margin-top: -15px;">DeltaJordan</em>
			<div>
				<sl-icon name="github" @click="${ this.openGithub }"></sl-icon>
				<sl-icon src="/assets/icons/gitlab.svg" @click="${ this.openGitlab }"></sl-icon>
				<sl-icon name="linkedin" @click="${ this.openLinkedIn }"></sl-icon>
			</div>
			<h2>Check out some of my projects:</h2>
			<table>
				<tr>
					<td>
						<h3>Synestia</h3>
						<div>
							<sl-icon name="twitter" @click="${ this.openTwitter }"></sl-icon>
							<sl-icon src="/assets/icons/bluesky.svg" @click="${ this.openBlueSky }"></sl-icon>
						</div>
					</td>
					<td>
						<h3>Self-Hosted 4get Instance</h3>
						<sl-icon name="search-heart" @click="${ this.open4get }"></sl-icon>
					</td>
				</tr>
			</table>
		`;
	}

	open4get(): void {
		window.open('https://whoogle.kelpdo.me', '_blank');
	}

	openBlueSky(): void {
		window.open('https://bsky.app/profile/papergarlic.bsky.social', '_blank');
	}

	openTwitter(): void {
		window.open('https://twitter.com/papergarlicyum', '_blank');
	}

	openLinkedIn(): void {
		window.open('https://www.linkedin.com/in/john-strebeck-6355251a1/', '_blank');
	}

	openGithub(): void {
		window.open('https://github.com/DeltaJordan', '_blank');
	}

	openGitlab(): void {
		window.open('https://gitlab.com/DeltaJordan', '_blank');
	}
}
