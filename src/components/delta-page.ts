import { css, CSSResultGroup, html } from 'lit';
import { DeltaElement } from '../delta-element';
import { customElement, state } from 'lit/decorators.js';
import { DeltaNavEvent } from '../events/delta-nav-event';
import './delta-router';
import './delta-navbar';

@customElement('delta-page')
export class DeltaPage extends DeltaElement {
	@state()
	path: string = document.location.pathname;

	@state()
	show404 = false;

	@state()
	ready = true;

	constructor() {
		super();
		this._navigate = this._navigate.bind(this);
		this._redirect404 = this._redirect404.bind(this);
		this._onHistoryNav = this._onHistoryNav.bind(this);
	}



	override connectedCallback() {
		super.connectedCallback();
		this.addEventListener('delta-nav-event', this._navigate);
		this.addEventListener('delta-404-event', this._redirect404);
		window.addEventListener('popstate', this._onHistoryNav);
	}

	override disconnectedCallback() {
		window.removeEventListener('popstate', this._onHistoryNav);
		this.removeEventListener('delta-404-event', this._redirect404);
		this.removeEventListener('delta-nav-event', this._navigate);
		super.disconnectedCallback();
	}

	static override get styles(): CSSResultGroup {
		return css`

			:host {
				font-family: var(--sl-font-sans);
				display: flex;
				flex-direction: column;
				align-items: stretch;
				width: 100%;
				min-height: 100vh;
			}

			delta-navbar {
				position: sticky;
				flex-shrink: 1;
				flex-grow: 0;
				top: 0;
				left: 0;
				z-index: calc( var(--sl-z-index-dialog) - 1 );
			}

			sl-spinner {
				margin-top: 25vh;
				align-self: center;
				font-size: 120px;
				--track-width: 6px;
			}
		`;
	}

	protected override render(): unknown {
		if (!this.ready) {
			return html`<sl-spinner></sl-spinner>`;
		}

		return html`
			<delta-router></delta-router>
		`;
	}

	_navigate(event: DeltaNavEvent): void {
		window.history.pushState({}, '', event.detail.route);
		this.path = event.detail.route.split(/\?|#/, 1)[0];
	}

	_redirect404(): void {
		this.show404 = true;
		document.getElementById('meta-robots')?.setAttribute('content', 'noindex');
	}

	_onHistoryNav(): void {
		this.path = document.location.pathname;
	}
}
