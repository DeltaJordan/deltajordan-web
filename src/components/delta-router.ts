import { css, CSSResultGroup, html } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { DeltaElement } from '../delta-element';
import '../pages/delta-home-page';

@customElement('delta-router')
export class DeltaRouter extends DeltaElement {
	@property({ attribute: false })
	path: string = document.location.pathname;

	@property({ attribute: 'not-found', type: Boolean })
	notFound = false;

	static override get styles(): CSSResultGroup {
		return css`
			:host {
				position: relative;
			}

			* {
				box-sizing: border-box;
				width: 100%;
			}
		`;
	}

	protected override render(): unknown {
		if (this.notFound) return html`<delta-404-page></delta-404-page>`;

		if (!this.path || this.path === '/' || this.path === '/index.html') {
			return html`<delta-home-page></delta-home-page>`;
		}

		return html`<delta-404-page></delta-404-page>`;
	}
}
