import stylistic from '@stylistic/eslint-plugin';
import { FlatCompat } from '@eslint/eslintrc';
import path from 'path';
import { fileURLToPath } from 'url';
import globals from 'globals';
import js from '@eslint/js';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const compat = new FlatCompat( { resolvePluginsRelativeTo: __dirname } );

export default [
	...compat.plugins('@typescript-eslint'),
	...compat.extends('plugin:@typescript-eslint/eslint-recommended', 'plugin:@typescript-eslint/recommended'),
	...compat.config({
		parser: '@typescript-eslint/parser'
	}),
	js.configs.recommended,
	{
		plugins: {
			'@stylistic': stylistic
		},
		languageOptions: {
			ecmaVersion: 2022,
			sourceType: 'module',
			globals: {
				...globals.browser
			}
		},
		rules: {
			'no-prototype-builtins': 'off',
			'@typescript-eslint/ban-types': 'off',
			'@typescript-eslint/explicit-function-return-type': 'off',
			'@typescript-eslint/explicit-module-boundary-types': 'off',
			'@typescript-eslint/no-explicit-any': 'error',
			'@typescript-eslint/no-empty-function': 'off',
			'@typescript-eslint/no-non-null-assertion': 'off',
			'@typescript-eslint/no-unused-vars': 'warn',
			'@typescript-eslint/no-this-alias': 'off',

			'no-debugger': 'warn',
			'no-fallthrough': 'off',
			'no-unreachable': 'warn',
			'no-unused-vars': 'warn',

			'consistent-return': 'error',
			'default-case-last': 'error',
			'eqeqeq': [
				'error',
				'always',
				{ 'null': 'ignore' }
			],
			'no-alert': 'warn',
			'no-console': ['warn', { 'allow': ['warn', 'error'] }],
			'no-empty': 'off',
			'no-eval': 'error',
			'no-implied-eval': 'error',
			'no-lonely-if': 'warn',
			'no-var': 'error',
			'no-constant-condition': ['error', { 'checkLoops': false }],
			'prefer-const': 'warn',

			'brace-style': ['error', '1tbs', { 'allowSingleLine': true }],
			'eol-last': ['error', 'always'],
			'func-call-spacing': ['error', 'never'],
			'indent': ['error', 'tab', {
				'ignoredNodes': [
					'PropertyDefinition',
					'SwitchCase'
				]
			}],
			'keyword-spacing': [
				'error',
				{
					'before': true,
					'after': true
				}
			],
			'linebreak-style': ['error', 'unix'],
			'no-extra-parens': 'off',
			'no-trailing-spaces': [
				'error',
				{ 'skipBlankLines': true }
			],
			'space-in-parens': 'off',
			'unicode-bom': ['error', 'never'],

			'@stylistic/semi': ['error', 'always'],
			'@stylistic/quotes': ['error', 'single'],
			'@stylistic/template-curly-spacing': ['error', 'always']
		}
	}
];

