import { defineConfig, PluginOption } from 'vite';
import { viteStaticCopy } from 'vite-plugin-static-copy';
import copy from 'rollup-plugin-copy';
import tsconfig from './tsconfig.json';

export default defineConfig(({ command, mode }) => {
	const plugins: PluginOption = [];

	if (command === 'build') {
		plugins.push(
			copy({
				targets: [{
					src: mode === 'development' ? 'robots.txt.dev' : 'robots.txt.prod',
					dest: 'dist',
					rename: 'robots.txt'
				}],
				copyOnce: true,
				overwrite: true,
				hook: 'writeBundle'
			})
		);
		plugins.push(
			copy({
				targets: [{
					src: 'node_modules/@shoelace-style/shoelace/dist/assets',
					dest: 'dist/shoelace',
				}],
				copyOnce: true,
				recursive: true,
				hook: 'writeBundle'
			})
		);
	} else {
		plugins.push(
			viteStaticCopy({
				targets: [{
					src: 'node_modules/@shoelace-style/shoelace/dist/assets/**',
					dest: 'shoelace/assets',
				}]
			})
		);
	}

	return {
		build: {
			target: tsconfig.compilerOptions.target,
			sourcemap: mode === 'development' ? 'inline' : false,
			chunkSizeWarningLimit: 1024
		},
		server: {
			port: 3200,
			strictPort: true,
			open: true
		},
		preview: {
			port: 3200
		},
		define: {
		},
		plugins
	};
});
